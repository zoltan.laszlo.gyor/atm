package person;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import financial.bank.Bank;

public class Customer {
	private String name;
	private int birthYear;
	private Bank bank;
	private int amount;
	
	private Customer(String name, int birthYear, Bank bank) {
		this.name = name;
		this.birthYear = birthYear;
		this.bank = bank;
		this.amount = 0;
	}

	public static Customer makeCustomer(String name, int birthYear, String bankName) {
		for (int i = 0; i < name.length(); i++) { //check for 2 spaces next to each other
			if(name.charAt(i) == ' ') {
				if(name.charAt(i+1) == ' ') {
					return null;
				}
			}
		}
		String[] splittedName = name.split(" "); //check if the String name has less than 2 or more 4 names
		if(splittedName.length < 2 || splittedName.length > 4) {
			return null;
		}
		
		Pattern p = Pattern.compile("[^A-Za-z]"); //check if the String contains any special characters
		Matcher m;
		boolean check; /*= false;*/
		for (int i = 0; i < splittedName.length; i++) {
			m = p.matcher(splittedName[i]);
			check = m.find();
			if(check) {
				return null;
			}
		}
		
		for (int i = 0; i < splittedName.length; i++) { //check if any of the names contains at least 3 characters
			if(splittedName[i].length() < 3) {
				return null;
			}
		}
		
		for (int i = 0; i < splittedName.length; i++) { // check if the names start with upper case and all of their characters except the 1st one are lower case 
			if(Character.isUpperCase(splittedName[i].charAt(0)) == false) {
				return null;
			}
			for (int j = 1; j < splittedName[i].length(); j++) {
				if(Character.isLowerCase(splittedName[i].charAt(j)) == false) {
					return null;
				}
			}
		}
		
		if(birthYear < 1918 || birthYear > 1998) { //check if the year is good
			return null;
		}
		
		try { //Check if the bankName is good
			Bank.valueOf(bankName);
		} catch (IllegalArgumentException e) {
			return null;
		}
		
		return new Customer(name, birthYear, Bank.valueOf(bankName));
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public void decreaseAmount(int amount) {
		this.amount -= amount;
	}
	
	public void increaseAmount(int amount) {
		this.amount += amount;
	}
	public String toString() {
		return this.name+": " + this.amount + "\n";
	}
	
	
}
