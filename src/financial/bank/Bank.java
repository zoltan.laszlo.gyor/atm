package financial.bank;

public enum Bank {
	BB,
	OTP,
	Erste,
	CIB,
	Raiffeisen,
	Citybank,
	FHB,
	MKB,
	UniCredit;
}
