package financial.bank.ATM;

import financial.bank.Bank;
import person.Customer;

public class ATM {
	private Bank bank;
	private int amount;
	
	private ATM(Bank bank, int amount) {
		this.bank = bank;
		this.amount = amount;
	}
	
	public static ATM makeATM(String bankName, int amount) {
		try {
			Bank.valueOf(bankName);
		} catch (Exception e) {
			return null;
		}
		if(amount < 1) {
			return null;
		}
		return new ATM(Bank.valueOf(bankName), amount);
	}
	
	public int calculateFee(Bank bank, int value) {
		int fee = 0;
		if(bank == this.bank && value >= 200) {
			fee = (int) Math.ceil(value*0.01);
		}else if(bank != this.bank && value >= 500){
			fee = (int) Math.ceil(value*0.03);
		}else {
			return fee;
		}
		return fee;
	}
	
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public void decreaseAmount(int amount) {
		this.amount -= amount;
	}
	
	public void increaseAmount(int amount) {
		this.amount += amount;
	}
	
	
}
