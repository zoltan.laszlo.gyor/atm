package financial;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import financial.bank.Bank;
import financial.bank.ATM.ATM;
import person.Customer;

public class Simulator {
	private ATM atm;
	private ArrayList<Customer> customers;
	private PrintWriter pwLog;
	
	public Simulator(String bankName, int initAmount, String outputFileName) throws Exception {
		this.atm = ATM.makeATM(bankName, initAmount);
		if(this.atm == null) {
			throw new IllegalArgumentException("A bank neve vagy a kezd��rt�k nem felel meg");
		}
		this.customers = new ArrayList<Customer>();
		try {
			this.pwLog = new PrintWriter(outputFileName);
		} catch (Exception e) {
			throw e;
		}
		
	}
	
	private Customer getCustomerByName(String name) {
		/*boolean found = false;
		int i = 0;
		Customer theRightOne = null;
		while(found == false) {
			if(customers.get(i).getName().equals(name)) {
				theRightOne = customers.get(i);
				found = true;
			}
			i++;
		}
		return theRightOne;*/
		for (Customer tmp : customers) {
			if(name.equals(tmp.getName())) {
				return tmp;
			}
		}
		return null;
	}
	
	public void insertCustomer(String customerName, int birthYear, String bankName) {
		if(this.getCustomerByName(customerName) != null) {
			return;
		}
		Customer newCustomer = Customer.makeCustomer(customerName, birthYear, bankName);
		if(newCustomer == null) {
			return;
		}
		customers.add(newCustomer);
	}
	
	public void withdrawCash(String customerName, int amount){
		Customer customer = this.getCustomerByName(customerName);
		if(amount > this.atm.getAmount() || amount <= 0 || customer == null) {
			return;
		}
		int fee = this.atm.calculateFee(customer.getBank(), amount);
		if(fee + amount > customer.getAmount()) {
			return;
		}
		this.atm.decreaseAmount(amount);
		customer.decreaseAmount(fee+amount);
		pwLog.print(customer.toString());
	}
	
	public void depositCash(String customerName, int amount) {
		Customer customer = this.getCustomerByName(customerName);
		if(customer == null || amount <= 0) {
			return;
		}
		this.atm.increaseAmount(amount);
		customer.increaseAmount(amount);
		pwLog.print(customer.toString());
	}
	
	public void simulate(String inputFileName) throws FileNotFoundException, NumberFormatException{
		try {
			Scanner sc = new Scanner(new File(inputFileName));
			String line = null;
			String[] data = null;
			String[] data2 = null;
			while(sc.hasNextLine()) {
				line = sc.nextLine();
				data = line.split(":");
				if(data.length == 2) {
					data2 = data[1].split(",");
					if(data[0].equals("REG") && data2.length == 3 && isNumber(data2[1]) ) {
						insertCustomer(data2[0], Integer.parseInt(data2[1]), data2[2]);
					}else if(data[0].equals("GET") && data2.length == 2 && isNumber(data2[1]) ) {
						withdrawCash(data2[0], Integer.parseInt(data2[1]));
					}else if(data[0].equals("PUT") && data2.length == 2 && isNumber(data2[1]) ){
						depositCash(data2[0], Integer.parseInt(data2[1]));
					}
				}
			}
		} catch (FileNotFoundException e ) {
			throw e;
		} catch (NumberFormatException e) {
			throw e;
		}
	}
	
	private boolean isNumber(String number) {
		try {
			Integer.parseInt(number);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public void close() {
		pwLog.close();
	}
	
	public ATM getAtm() {
		return atm;
	}
	public void setAtm(ATM atm) {
		this.atm = atm;
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public PrintWriter getPwLog() {
		return pwLog;
	}

	public void setPwLog(PrintWriter pwLog) {
		this.pwLog = pwLog;
	}
	
}
