package main;

import financial.Simulator;

public class Main {
	public static void main(String[] args) {
		if(args.length == 3) {
			try {
				Simulator simulator = new Simulator(args[0], 1000000, args[2]);
				simulator.simulate(args[1]);
				simulator.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			System.out.println("Error");
		}
		
	}
}